{-# OPTIONS_GHC -Wall #-}

module Main where

import qualified Interactive.TUI.Main as TUI

main :: IO ()
main = TUI.main
