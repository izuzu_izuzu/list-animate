{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

{-|
    @tail :: [a] -> [a]@
-}
module Animations.Tail (main, fixedAnimation, dynamicAnimation) where

import Control.Lens ((%~), (-~), (.~), (^.), element, mapped, _last, _tail)
import Control.Monad ((<=<))
import Data.Foldable (traverse_)
import Data.List (genericLength)
import Data.Text (Text)
import qualified Data.Text as T
import Linear (_y)
import Text.RawString.QQ (r)

import Reanimate hiding (cubicBezierS)
import Reanimate.Builtin.Documentation (docEnv)
import Reanimate.Scene
    ( Object
    , oContext
    , oDraw
    , oFadeIn
    , oFadeOut
    , oModify
    , oNew
    , oRead
    , oTranslate
    , oTween
    , oZIndex
    )

import Utilities

data Case = General | EmptyXs

main :: IO ()
main = reanimate fixedAnimation

{-
-------------------------------------------------------------------------------
    Fixed animation
-------------------------------------------------------------------------------
-}

fixedAnimation :: Animation
fixedAnimation = prepareAnimation $ scene $ do
    wait 1
    fork typeSigScene
    wait 0.25
    play . transitions $ scene $ caseScene General >> wait 3
    wait 0.5
    caseScene EmptyXs
    wait 3

typeSigScene :: Scene s ()
typeSigScene = do
    let typeSigSvg = latexBoldFg . lockLatexSpaces . T.concat $ typeSigChunks
    typeSig <- oNew typeSigSvg
    oMoveTo typeSig (0, 2)
    oShowWith typeSig 1 oDraw

caseScene :: Case -> Scene s ()
caseScene General = do
    let
        funcDefSvgs = latexBoldFgChunks funcDefChunks

        xs =
            [ "x\\textsubscript{0}"
            , "x\\textsubscript{1}"
            , "..."
            , "x\\textsubscript{n-2}"
            , "x\\textsubscript{n-1}"
            ]

        result = tail xs

        [xsExprSvg, resultExprSvg] = zipWith
            (\color -> latexRegularWith (withColor color) . makeListExpr)
            [xsColor, resultColor]
            [xs, result]

        xsBoxesWidths = [1, 1, 3, 1, 1]

        xsBoxesSvgs = customListBoxes xsBoxesWidths xsColor

        xsLabelsSvgs = customListLabels xsBoxesWidths xsColor xs

        hSep = 1.5

        showPosY = -0.5

    funcDefSplit@(~[funcDefName, funcDefXs]) <- traverse oNew funcDefSvgs
    traverse_ (`oMoveToY` 1) funcDefSplit

    xsExpr <- oNew xsExprSvg
    xsBoxes@(~(xsHeadBox : xsTailBoxes)) <- traverse
        oNewWithSvgPositionX
        xsBoxesSvgs
    xsLabels@(~(xsHeadLabel : xsTailLabels)) <- traverse
        oNewWithSvgPositionX
        xsLabelsSvgs

    resultExpr <- oNew resultExprSvg

    traverse_
        (`oMoveToY` showPosY)
        (resultExpr : xsExpr : xsBoxes ++ xsLabels)

    let
        showFuncDef = let d = 1 in waitOn $ do
            oShowAsGroupWith funcDefSplit d oDraw

        showXs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith xsExpr d oDraw
                , oTweenColor funcDefXs d fgColor xsColor
                ]
            wait (d/2)
            oFadeSlide [xsExpr] (xsBoxes ++ xsLabels) (d/2)

        splitHead = let d = 1 in
            oWithEasing snapOutS (xsBoxes ++ xsLabels)
            . waitOn
            . traverse_ fork
            $ fmap
                (\obj -> oTweenMoveBy obj d (-hSep/2, 0))
                [xsHeadBox, xsHeadLabel]
            ++ fmap
                (\obj -> oTweenMoveBy obj d (hSep/2, 0))
                (xsTailBoxes ++ xsTailLabels)

        focusTail = let d = 1 in waitOn $ do
            tailBoxesCenterX <-
                svgCenterX . mkGroup <$> traverse oRender xsTailBoxes
            waitOn
                . traverse_ fork
                $ fmap
                    (\obj -> oHideWith obj (d/2) oFadeOut)
                    [xsHeadBox, xsHeadLabel]
                ++ fmap
                    (\obj -> oTweenMoveBy obj d (-tailBoxesCenterX, 0))
                    (xsTailBoxes ++ xsTailLabels)

        highlightResult = let d = 1 in waitOn . traverse_ fork
            $ oTweenColor funcDefName d fgColor resultColor
            : fmap
                (\obj -> oTweenColor obj d xsColor resultColor)
                (funcDefXs : xsBoxes ++ xsLabels)

        showResultExpr = let d = 0.5 in waitOn $ do
            oFadeSlide (xsBoxes ++ xsLabels) [resultExpr] d

    showFuncDef
    wait 0.5
    showXs
    wait 1
    splitHead
    fork highlightResult
    focusTail
    wait 1
    showResultExpr

caseScene EmptyXs = do
    let
        funcDefSvgs = latexBoldFgChunks . (element 1 .~ " []") $ funcDefChunks

        funcDefNoteSvg = latexRegularWith
            (scale (2/3))
            [r|\it{(Special case: \emph{xs} is empty.)}|]

        xsExprSvg = latexRegularWith (withColor xsColor) $ makeListExpr []

        xsBoxSvg = emptyListBox xsColor

        errorMessageSvg = latexBoldWith
            (withColor errorColor)
            [r|error \textlf{"empty list"}|]

        showPosY = -1

    funcDefSplit@(~[funcDefName, funcDefXs]) <- traverse oNew funcDefSvgs
    traverse_ (`oMoveToY` 1) funcDefSplit

    funcDefNote <- oNew funcDefNoteSvg
    oMoveToY funcDefNote 0.25

    ~[xsExpr, xsBox, errorMessage] <- traverse
        oNew
        [xsExprSvg, xsBoxSvg, errorMessageSvg]
    traverse_ (`oMoveToY` showPosY) [xsExpr, xsBox, errorMessage]

    let
        showFuncDefWithNote = let d = 1 in waitOn $ do
            fork $ oShowAsGroupWith funcDefSplit d oDraw
            wait (d/4)
            oShowWith funcDefNote d oDraw

        showXs = let d = 1 in waitOn $ traverse_ fork
            [ oShowWith xsBox d oDraw
            , oTweenColor funcDefXs d fgColor xsColor
            ]

        highlightFuncDefError = let d = 0.5 in waitOn $ do
            funcDefWidth <-
                svgWidth . mkGroup <$> traverse oRender funcDefSplit
            highlight <- oNew
                . center
                . withColor errorColor
                . withOpacity 1
                $ mkRectFull (funcDefWidth + 0.5) 0.75 0.25 0.25
            oMoveToY highlight 1
            oModify highlight $ oZIndex -~ 3
            waitOn $ traverse_ fork
                [ oShowWith highlight d oFadeIn
                , oTweenColor funcDefName d fgColor bgColor
                , oTweenColor funcDefXs d xsColor bgColor
                ]

        showError = let d = 0.5 in waitOn $ do
            oFadeSlide [xsBox] [errorMessage] d

    showFuncDefWithNote
    wait 0.5
    showXs
    wait 1
    fork highlightFuncDefError
    showError

{-
-------------------------------------------------------------------------------
    Dynamic animation
-------------------------------------------------------------------------------
-}

dynamicAnimation :: String -> [String] -> Animation
dynamicAnimation typeSigStr xs@(_:_) = prepareAnimation $ scene $ do
    let
        result = tail xs

        typeSigSvg =
            centerX
            . latexBoldFg
            . T.concat
            . (_last .~ " " <> T.pack typeSigStr)
            $ typeSigChunks

        funcDefSvgs = centerGroupX $ latexBoldFgChunks funcDefChunks

        [xsExprSvg, resultExprSvg] = zipWith
            (\color ->
                centerX
                . latexRegularWith (withColor color . scale reducedTextScale)
                . makeListExpr
                . fmap T.pack)
            [xsColor, resultColor]
            [xs, result]

        xsBoxWidth =
            max 1
            . min ((maxFrameWidth - hSep) / genericLength xs)
            . (\svgs -> 0.5 + maximum . (0 :) $ svgWidth <$> svgs)
            $ customListLabelsWith
                (0 <$ xs)
                (scale reducedTextScale . withDefaultTextScaleStrokeFill)
                (lockLatexSpaces . T.pack <$> xs)

        xsBoxesWidths = xsBoxWidth <$ xs

        xsBoxesSvgs = case xs of
            [_] -> zipWith
                (`translate` 0)
                (distribute1D $ xsBoxesWidths ++ [0])
                (boxes ++ [emptyListBox xsColor])
            _ -> boxes
            where
                boxes = customListBoxesWith
                    xsBoxesWidths
                    (withColor xsColor . withDefaultLineStrokeFill)

        xsLabelsSvgs = case xs of
            [_] -> labels ++ [mkPath []]
            _ -> labels
            where
                labels = customListLabelsWith
                    xsBoxesWidths
                    ( centerX
                    . withColor xsColor
                    . scale reducedTextScale
                    . withDefaultTextScaleStrokeFill
                    )
                    (lockLatexSpaces . T.pack <$> xs)

        xsTextContainerFunc = TextContainerSVG
            (xsBoxWidth - 2*boxMargin, 1 - 2*boxMargin)
            ( maximum . (0 :) $ svgWidth <$> xsLabelsSvgs
            , maximum . (0 :) $ svgHeight <$> xsLabelsSvgs
            )
            1

        hSep = 1.5

        boxMargin = 0.15

        reducedTextScale = 0.8

        showPosY = -0.5

    typeSig <- oNew typeSigSvg
    oMoveTo typeSig (0, 2)

    funcDefSplit@(~[funcDefName, funcDefXs]) <- traverse oNew funcDefSvgs
    traverse_ (`oMoveToY` 1) funcDefSplit

    xsExpr <- oNew xsExprSvg
    xsBoxes@(~(xsHeadBox : xsTailBoxes)) <- traverse
        oNewWithSvgPositionX
        xsBoxesSvgs
    xsLabels@(~(xsHeadLabel : xsTailLabels)) <- traverse
        (oCloneModifyVal xsTextContainerFunc <=< oNewWithSvgPositionX)
        xsLabelsSvgs

    resultExpr <- oNew resultExprSvg

    traverse_ (`oMoveToY` showPosY) (resultExpr : xsExpr : xsBoxes)
    traverse_ (`oMoveToY` showPosY) xsLabels

    let
        showTypeSig = let d = 1 in waitOn $ do
            oShowWith typeSig d oDraw

        showFuncDef = let d = 1 in waitOn $ do
            oShowAsGroupWith funcDefSplit d oDraw

        showXs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith xsExpr d oDraw
                , oTweenColor funcDefXs d fgColor xsColor
                ]
            wait (d/2)
            waitOn . traverse_ fork $ case xs of
                [_] ->
                    [ oFadeSlide [xsExpr] [xsHeadBox] (d/2)
                    , oFadeSlide [] [xsHeadLabel] (d/2)
                    ]
                _ ->
                    [ oFadeSlide [xsExpr] xsBoxes (d/2)
                    , oFadeSlide [] xsLabels (d/2)
                    ]

        splitHead = let d = 1 in
            oWithEasing snapOutS xsBoxes
            . oWithEasing snapOutS xsLabels
            . waitOn
            . traverse_ fork
            $ fmap
                (\obj -> oTweenMoveBy obj d (-hSep/2, 0))
                [xsHeadBox]
            ++ fmap
                (\obj -> oTweenMoveBy obj d (-hSep/2, 0))
                [xsHeadLabel]
            ++ fmap
                (\obj -> oTweenMoveBy obj d (hSep/2, 0))
                xsTailBoxes
            ++ fmap
                (\obj -> oTweenMoveBy obj d (hSep/2, 0))
                xsTailLabels
            ++ case xs of
                [_] -> fmap (\obj -> oShowWith obj (d*2/3) oFadeIn) xsTailBoxes
                _ -> []

        focusTail = let d = 1 in waitOn $ do
            tailBoxesCenterX <-
                svgCenterX . mkGroup <$> traverse oRender xsTailBoxes
            waitOn
                . traverse_ fork
                $ fmap
                    (\obj -> oHideWith obj (d/2) oFadeOut)
                    [xsHeadBox]
                ++ fmap
                    (\obj -> oHideWith obj (d/2) oFadeOut)
                    [xsHeadLabel]
                ++ fmap
                    (\obj -> oTweenMoveBy obj d (-tailBoxesCenterX, 0))
                    xsTailBoxes
                ++ fmap
                    (\obj -> oTweenMoveBy obj d (-tailBoxesCenterX, 0))
                    xsTailLabels

        highlightResult = let d = 1 in waitOn . traverse_ fork
            $ oTweenColor funcDefName d fgColor resultColor
            : fmap
                (\obj -> oTweenColor obj d xsColor resultColor)
                (funcDefXs : xsBoxes)
            ++ fmap
                (\obj -> oTweenColor obj d xsColor resultColor)
                xsLabels

        showResultExpr = let d = 0.5 in waitOn $ traverse_ fork
            [ oFadeSlide xsBoxes [resultExpr] d
            , oFadeSlide xsLabels [] d
            ]

    wait 1

    fork showTypeSig
    wait 0.25

    showFuncDef
    wait 0.5
    showXs
    wait 1
    splitHead
    fork highlightResult
    focusTail
    wait 1
    showResultExpr

    wait 3

dynamicAnimation typeSigStr [] = prepareAnimation $ scene $ do
    let
        typeSigSvg =
            centerX
            . latexBoldFg
            . T.concat
            . (_last .~ " " <> T.pack typeSigStr)
            $ typeSigChunks

        funcDefSvgs = latexBoldFgChunks funcDefChunks

        xsExprSvg = latexRegularWith (withColor xsColor) $ makeListExpr []

        xsBoxSvg = emptyListBox xsColor

        errorMessageSvg = latexBoldWith
            (withColor errorColor)
            [r|error \textlf{"empty list"}|]

        showPosY = -0.5

    typeSig <- oNew typeSigSvg
    oMoveTo typeSig (0, 2)

    funcDefSplit@(~[funcDefName, funcDefXs]) <- traverse oNew funcDefSvgs
    traverse_ (`oMoveToY` 1) funcDefSplit

    ~[xsExpr, xsBox, errorMessage] <- traverse
        oNew
        [xsExprSvg, xsBoxSvg, errorMessageSvg]
    traverse_ (`oMoveToY` showPosY) [xsExpr, xsBox, errorMessage]

    let
        showTypeSig = let d = 1 in waitOn $ do
            oShowWith typeSig d oDraw

        showFuncDef = let d = 1 in waitOn $ do
            fork $ oShowAsGroupWith funcDefSplit d oDraw

        showXs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith xsExpr d oDraw
                , oTweenColor funcDefXs d fgColor xsColor
                ]
            wait (d/2)
            oFadeSlide [xsExpr] [xsBox] (d/2)

        highlightFuncDefError = let d = 0.5 in waitOn $ do
            funcDefWidth <-
                svgWidth . mkGroup <$> traverse oRender funcDefSplit
            highlight <- oNew
                . center
                . withColor errorColor
                . withOpacity 1
                $ mkRectFull (funcDefWidth + 0.5) 0.75 0.25 0.25
            oMoveToY highlight . (^. _y) =<< oRead funcDefName oTranslate
            oModify highlight $ oZIndex -~ 3
            waitOn $ traverse_ fork
                [ oShowWith highlight d oFadeIn
                , oTweenColor funcDefName d fgColor bgColor
                , oTweenColor funcDefXs d xsColor bgColor
                ]

        showError = let d = 0.5 in waitOn $ do
            oFadeSlide [xsBox] [errorMessage] d

    wait 1

    fork showTypeSig
    wait 0.25

    showFuncDef
    wait 0.5
    showXs
    wait 1
    fork highlightFuncDefError
    showError

    wait 3

{-
-------------------------------------------------------------------------------
    Animation-specific functions and values
-------------------------------------------------------------------------------
-}

typeSigChunks :: [Text]
typeSigChunks = _tail . mapped %~ (" " <>) $ ["tail", "::", "[a] -> [a]"]

funcDefChunks :: [Text]
funcDefChunks = _tail . mapped %~ (" " <>) $ ["tail", "xs"]

{-
-------------------------------------------------------------------------------
    Helper functions and values
-------------------------------------------------------------------------------
-}

env :: Animation -> Animation
env =
    docEnv
    . addStatic (mkBackground bgColor)
    -- . addStatic (withStrokeOpacity 0.25 $ mkBackgroundGrid 1 1)
    -- . addStatic (withStrokeOpacity 0.25 mkBackgroundAxes)

transitions :: Animation -> Animation
transitions = applyE (overEnding 1 fadeOutE)

prepareAnimation :: Animation -> Animation
prepareAnimation =
    env
    . transitions
    . fitAnimationToSize (maxFrameWidth, maxFrameHeight)

maxFrameWidth :: Double
maxFrameWidth = screenWidth - 1

maxFrameHeight :: Double
maxFrameHeight = screenHeight - 1

fgColor :: String
fgColor = "black"

bgColor :: String
bgColor = "floralwhite"

xsColor :: String
xsColor = "red"

resultColor :: String
resultColor = "magenta"

errorColor :: String
errorColor = "crimson"

withDefaultTextScaleStrokeFill :: SVG -> SVG
withDefaultTextScaleStrokeFill =
    withDefaultTextStrokeFill . withDefaultTextScale

withAllSubglyphs :: (SVG -> SVG) -> SVG -> SVG
withAllSubglyphs = withSubglyphs [0 ..]

lockLatexSpaces :: Text -> Text
lockLatexSpaces = T.replace " " "~"

makeListExpr :: [Text] -> Text
makeListExpr =
    (<> "]")
    . ("\\raggedright[" <>)
    . T.intercalate ", "
    . fmap lockLatexSpaces

latexBoldChunksWith :: (SVG -> SVG) -> [Text] -> [SVG]
latexBoldChunksWith transformation =
    centerGroupX
    . latexCfgChunksAlignedYWith
        defaultBoldTextCfg
        (transformation . withDefaultTextScaleStrokeFill)

latexBoldWith :: (SVG -> SVG) -> Text -> SVG
latexBoldWith transformation =
    centerX
    . latexCfgAlignedYWith
        defaultBoldTextCfg
        (transformation . withDefaultTextScaleStrokeFill)

latexBoldFgChunks :: [Text] -> [SVG]
latexBoldFgChunks = latexBoldChunksWith (withColor fgColor)

latexBoldFg :: Text -> SVG
latexBoldFg = latexBoldWith (withColor fgColor)

latexRegularWith :: (SVG -> SVG) -> Text -> SVG
latexRegularWith transformation =
    centerX
    . latexCfgAlignedYWith
        defaultRegularTextCfg
        (transformation . withDefaultTextScaleStrokeFill)

oTweenColor :: Object s a -> Duration -> String -> String -> Scene s ()
oTweenColor obj d start end = oTween obj d $ \t -> oContext . mapped %~
    withAllSubglyphs (withTweenedColor start end t)

oFadeSlide :: [Object s a] -> [Object s a] -> Duration -> Scene s ()
oFadeSlide startObjects endObjects d = do
    traverse_ (`oMoveBy` (0, -0.5)) endObjects
    waitOn . traverse_ fork
        $ fmap
            (\obj -> oHideWith obj (d*2/3) oFadeOut)
            startObjects
        ++ fmap
            (\obj -> oShowWith obj (d*2/3) oFadeIn)
            endObjects
        ++ fmap
            (\obj -> oTweenMoveBy obj d (0, 0.5))
            (startObjects ++ endObjects)
