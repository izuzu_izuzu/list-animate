{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}

{-|
    @(++) :: [a] -> [a] -> [a]@
-}
module Animations.Append (main, fixedAnimation, dynamicAnimation) where

import Control.Lens ((%~), (.~), (^.), mapped, _1, _last, _tail)
import Control.Monad ((<=<), join)
import Data.Foldable (traverse_)
import Data.List (genericLength)
import Data.Text (Text)
import qualified Data.Text as T
import Linear (Additive (lerp), V1 (V1), _x)

import Reanimate
import Reanimate.Builtin.Documentation (docEnv)
import Reanimate.Scene
    ( Object
    , oContext
    , oDraw
    , oFadeIn
    , oFadeOut
    , oNew
    , oTween
    , oValue
    )

import Utilities

main :: IO ()
main = reanimate fixedAnimation

{-
-------------------------------------------------------------------------------
    Fixed animation
-------------------------------------------------------------------------------
-}

fixedAnimation :: Animation
fixedAnimation = prepareAnimation $ scene $ do
    wait 1
    fork typeSigScene
    wait 0.25
    funcScene
    wait 3

typeSigScene :: Scene s ()
typeSigScene = do
    let typeSigSvg = latexBoldFg . lockLatexSpaces . T.concat $ typeSigChunks
    typeSig <- oNew typeSigSvg
    oMoveTo typeSig (0, 2)
    oShowWith typeSig 1 oDraw

funcScene :: Scene s ()
funcScene = do
    let
        funcDefSvgs = latexBoldFgChunks funcDefChunks

        xs =
            [ "x\\textsubscript{0}"
            , "x\\textsubscript{1}"
            , "..."
            , "x\\textsubscript{m-1}"
            ]

        ys =
            [ "y\\textsubscript{0}"
            , "y\\textsubscript{1}"
            , "..."
            , "y\\textsubscript{n-2}"
            , "y\\textsubscript{n-1}"
            ]

        [xsExprSvg, ysExprSvg, resultExprSvg] = zipWith
            makeListExprSvg
            [xsColor, ysColor, resultColor]
            [xs, ys, xs ++ ys]
            where
                makeListExprSvg color =
                    latexRegularWith (withColor color) . makeListExpr

        xsBoxesWidths = [1, 1, 2, 1]

        ysBoxesWidths = [1, 1, 3, 1, 1]

        [xsBoxesSvgs, ysBoxesSvgs] = zipWith
            customListBoxes
            [xsBoxesWidths, ysBoxesWidths]
            [xsColor, ysColor]

        [xsLabelsSvgs, ysLabelsSvgs] = zipWith3
            customListLabels
            [xsBoxesWidths, ysBoxesWidths]
            [xsColor, ysColor]
            [xs, ys]

        hSep = 2

        showPosY = -0.5

        [xsSnapStartPosX, _, ysSnapStartPosX] = distribute1D
            [sum xsBoxesWidths, hSep, sum ysBoxesWidths]

        snapStartPosY = -2

    funcDefSplit@(~[funcDefXs, funcDefName, funcDefYs]) <-
        traverse oNew funcDefSvgs
    traverse_ (`oMoveToY` 1) funcDefSplit

    ~[xsExpr, xsBoxes, xsLabels] <- traverse
        oNew
        [xsExprSvg, mkGroup xsBoxesSvgs, mkGroup xsLabelsSvgs]

    ~[ysExpr, ysBoxes, ysLabels] <- traverse
        oNew
        [ysExprSvg, mkGroup ysBoxesSvgs, mkGroup ysLabelsSvgs]

    resultExpr <- oNew resultExprSvg

    traverse_
        (`oMoveToY` showPosY)
        [xsExpr, xsBoxes, xsLabels, ysExpr, ysBoxes, ysLabels]

    oMoveTo resultExpr (0, -2)

    let
        showFuncDef = let d = 1 in waitOn $ do
            oShowAsGroupWith funcDefSplit d oDraw

        showXs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith xsExpr d oDraw
                , oTweenColor funcDefXs d fgColor xsColor
                ]
            wait (d/2)
            oFadeSlide [xsExpr] [xsBoxes, xsLabels] (d/2)

        moveXs = let d = 1 in waitOn . traverse_ fork $ fmap
            (\obj -> oTweenMoveTo obj d (xsSnapStartPosX, snapStartPosY))
            [xsBoxes, xsLabels]

        showYs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith ysExpr d oDraw
                , oTweenColor funcDefYs d fgColor ysColor
                ]
            wait (d/2)
            oFadeSlide [ysExpr] [ysBoxes, ysLabels] (d/2)

        moveYs = let d = 1 in waitOn . traverse_ fork $ fmap
            (\obj -> oTweenMoveTo obj d (ysSnapStartPosX, snapStartPosY))
            [ysBoxes, ysLabels]

        moveFuncDef = let d = 1 in waitOn . traverse_ fork $ fmap
            (\obj -> oTweenMoveBy obj d (0, -1))
            funcDefSplit

        snapXsYs = let d = 0.5 in
            oWithEasing snapInS [xsBoxes, xsLabels, ysBoxes, ysLabels]
            . waitOn
            . traverse_ fork
            $ fmap
                (\obj -> oTweenMoveBy obj d (hSep/2, 0))
                [xsBoxes, xsLabels]
            ++ fmap
                (\obj -> oTweenMoveBy obj d (-hSep/2, 0))
                [ysBoxes, ysLabels]

        highlightResult = let d = 0 in waitOn . traverse_ fork
            $ oTweenColor funcDefName d fgColor resultColor
            : fmap
                (\obj -> oTweenColor obj d xsColor resultColor)
                [funcDefXs, xsBoxes, xsLabels]
            ++ fmap
                (\obj -> oTweenColor obj d ysColor resultColor)
                [funcDefYs, ysBoxes, ysLabels]

        showResultExpr = let d = 0.5 in waitOn $ do
            oFadeSlide [xsBoxes, xsLabels, ysBoxes, ysLabels] [resultExpr] d

    showFuncDef
    wait 0.5
    showXs
    wait 0.5
    moveXs
    wait 0.5
    showYs
    wait 0.5
    fork moveYs
    moveFuncDef
    wait 0.5
    snapXsYs
    highlightResult
    wait 1.5
    showResultExpr

{-
-------------------------------------------------------------------------------
    Dynamic animation
-------------------------------------------------------------------------------
-}

dynamicAnimation :: String -> [String] -> [String] -> Animation
dynamicAnimation typeSigStr xs ys = prepareAnimation $ scene $ do
    let
        result = xs ++ ys

        typeSigSvg =
            centerX
            . latexBoldFg
            . T.concat
            . (_last .~ " " <> T.pack typeSigStr)
            $ typeSigChunks

        funcDefSvgs = centerGroupX $ latexBoldFgChunks funcDefChunks

        [xsExprSvg, ysExprSvg, resultExprSvg] = zipWith
            (\color ->
                centerX
                . latexRegularWith (withColor color . scale reducedTextScale)
                . makeListExpr
                . fmap T.pack)
            [xsColor, ysColor, resultColor]
            [xs, ys, result]

        [xsBoxWidth, ysBoxWidth, resultBoxWidth] = zipWith
            (\hSpace list ->
                max 1.5
                . min ((maxFrameWidth - hSpace) / genericLength list)
                . (\svgs -> 0.5 + maximum . (0 :) $ svgWidth <$> svgs)
                $ customListLabelsWith
                    (0 <$ list)
                    (scale reducedTextScale . withDefaultTextScaleStrokeFill)
                    (lockLatexSpaces . T.pack <$> list))
            [0, 0, hSep]
            [xs, ys, result]

        xsBoxesWidths = case xs of
            [] -> [0]
            _ -> xsBoxWidth <$ xs

        xsBoxesSvgs = case xs of
            [] -> [emptyListBox xsColor]
            _ -> customListBoxesWith
                xsBoxesWidths
                (withColor xsColor . withDefaultLineStrokeFill)

        xsLabelsSvgs = case xs of
            [] -> [mkPath []]
            _ -> customListLabelsWith
                xsBoxesWidths
                ( centerX
                . withColor xsColor
                . scale reducedTextScale
                . withDefaultTextScaleStrokeFill
                )
                (lockLatexSpaces . T.pack <$> xs)

        xsTextContainerFunc = TextContainerSVG
            (xsBoxWidth - 2*boxMargin, 1 - 2*boxMargin)
            ( maximum . (0 :) $ svgWidth <$> xsLabelsSvgs
            , maximum . (0 :) $ svgHeight <$> xsLabelsSvgs
            )
            1

        ysBoxesWidths = case ys of
            [] -> [0]
            _ -> ysBoxWidth <$ ys

        ysBoxesSvgs = case ys of
            [] -> [emptyListBox ysColor]
            _ -> customListBoxesWith
                ysBoxesWidths
                (withColor ysColor . withDefaultLineStrokeFill)

        ysLabelsSvgs = case ys of
            [] -> [mkPath []]
            _ -> customListLabelsWith
                ysBoxesWidths
                ( centerX
                . withColor ysColor
                . scale reducedTextScale
                . withDefaultTextScaleStrokeFill
                )
                (lockLatexSpaces . T.pack <$> ys)

        ysTextContainerFunc = TextContainerSVG
            (ysBoxWidth - 2*boxMargin, 1 - 2*boxMargin)
            ( maximum . (0 :) $ svgWidth <$> ysLabelsSvgs
            , maximum . (0 :) $ svgHeight <$> ysLabelsSvgs
            )
            1

        hSep = 2

        boxMargin = 0.15

        reducedTextScale = 0.8

        showPosY = -0.5

        [xsSnapStartPosX, _, ysSnapStartPosX] = distribute1D
            [sum $ resultBoxWidth <$ xs, hSep, sum $ resultBoxWidth <$ ys]

        snapStartPosY = -2

    typeSig <- oNew typeSigSvg
    oMoveTo typeSig (0, 2)

    funcDefSplit@(~[funcDefXs, funcDefName, funcDefYs]) <-
        traverse oNew funcDefSvgs
    traverse_ (`oMoveToY` 1) funcDefSplit

    xsExpr <- oNew xsExprSvg
    xsBoxes <- traverse
        (oCloneModifyVal (NonScalingStrokeSVG defaultStrokeWidth)
            <=< oNewWithSvgPositionX)
        xsBoxesSvgs
    xsLabels <- traverse
        (oCloneModifyVal xsTextContainerFunc <=< oNewWithSvgPositionX)
        xsLabelsSvgs

    ysExpr <- oNew ysExprSvg
    ysBoxes <- traverse
        (oCloneModifyVal (NonScalingStrokeSVG defaultStrokeWidth)
            <=< oNewWithSvgPositionX)
        ysBoxesSvgs
    ysLabels <- traverse
        (oCloneModifyVal ysTextContainerFunc <=< oNewWithSvgPositionX)
        ysLabelsSvgs

    resultExpr <- oNew resultExprSvg

    let
        (xsExpr_, xsBoxes_, xsLabels_) =
            (O_ xsExpr, fmap O_ xsBoxes, fmap O_ xsLabels)

        (ysExpr_, ysBoxes_, ysLabels_) =
            (O_ ysExpr, fmap O_ ysBoxes, fmap O_ ysLabels)

    traverse_
        (oApply_ (`oMoveToY` showPosY))
        (xsExpr_ : ysExpr_ : xsBoxes_ ++ ysBoxes_ ++ xsLabels_ ++ ysLabels_)

    oMoveToY resultExpr snapStartPosY

    let
        showTypeSig = let d = 1 in waitOn $ do
            oShowWith typeSig d oDraw

        showFuncDef = let d = 1 in waitOn $ do
            oShowAsGroupWith funcDefSplit d oDraw

        showXs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith xsExpr d oDraw
                , oTweenColor funcDefXs d fgColor xsColor
                ]
            wait (d/2)
            oFadeSlide_ [xsExpr_] (xsBoxes_ ++ xsLabels_) (d/2)

        moveXs = let d = 1 in waitOn . traverse_ fork
            $ fmap
                (oApply_ (\obj ->
                    oTweenMoveTo obj d (xsSnapStartPosX, snapStartPosY)))
                (xsBoxes_ ++ xsLabels_)
            ++ zipWith
                (\offsetX -> oApply_ (\obj -> oTweenMoveBy obj d (offsetX, 0)))
                (join (++) . distribute1D $ resultBoxWidth <$ xs)
                (xsBoxes_ ++ xsLabels_)
            ++ fmap
                (\obj -> oTween obj d $ \t -> oValue . strokedSvg %~
                    scaleXY (lerp' t (resultBoxWidth / xsBoxWidth) 1) 1)
                xsBoxes
            ++ fmap
                (\obj -> oTween obj d $ \t -> oValue . containerSize . _1 %~
                    lerp' t (resultBoxWidth - 2*boxMargin))
                xsLabels

        showYs = let d = 1 in waitOn $ do
            waitOn $ traverse_ fork
                [ oShowWith ysExpr d oDraw
                , oTweenColor funcDefYs d fgColor ysColor
                ]
            wait (d/2)
            oFadeSlide_ [ysExpr_] (ysBoxes_ ++ ysLabels_) (d/2)

        moveYs = let d = 1 in waitOn . traverse_ fork
            $ fmap
                (oApply_ (\obj ->
                    oTweenMoveTo obj d (ysSnapStartPosX, snapStartPosY)))
                (ysBoxes_ ++ ysLabels_)
            ++ zipWith
                (\offsetX -> oApply_ (\obj -> oTweenMoveBy obj d (offsetX, 0)))
                (join (++) . distribute1D $ resultBoxWidth <$ ys)
                (ysBoxes_ ++ ysLabels_)
            ++ fmap
                (\obj -> oTween obj d $ \t -> oValue . strokedSvg %~
                    scaleXY (lerp' t (resultBoxWidth / ysBoxWidth) 1) 1)
                ysBoxes
            ++ fmap
                (\obj -> oTween obj d $ \t -> oValue . containerSize . _1 %~
                    lerp' t (resultBoxWidth - 2*boxMargin))
                ysLabels

        moveFuncDef = let d = 1 in waitOn . traverse_ fork $ fmap
            (\obj -> oTweenMoveBy obj d (0, -1))
            funcDefSplit

        snapXsYs = let d = 0.5 in
            oWithEasing snapInS (xsBoxes ++ ysBoxes)
            . oWithEasing snapInS (xsLabels ++ ysLabels)
            . waitOn
            . traverse_ fork
            $ fmap
                (oApply_ (\obj -> oTweenMoveBy obj d (hSep/2, 0)))
                (xsBoxes_ ++ xsLabels_)
            ++ fmap
                (oApply_ (\obj -> oTweenMoveBy obj d (-hSep/2, 0)))
                (ysBoxes_ ++ ysLabels_)
            ++ fmap
                (oApply_
                    (\obj -> wait (d/3) >> oHideWith obj (d*2/3) oFadeOut))
                (case (xs, ys) of
                    ([], _:_) -> xsBoxes_ ++ xsLabels_
                    (_:_, []) -> ysBoxes_ ++ ysLabels_
                    _ -> [])

        highlightResult = let d = 0 in waitOn . traverse_ fork
            $ oTweenColor funcDefName d fgColor resultColor
            : fmap
                (oApply_
                    (\obj -> oTweenColor obj d xsColor resultColor))
                (O_ funcDefXs : xsBoxes_ ++ xsLabels_)
            ++ fmap
                (oApply_
                    (\obj -> oTweenColor obj d ysColor resultColor))
                (O_ funcDefYs : ysBoxes_ ++ ysLabels_)

        showResultExpr = let d = 0.5 in waitOn $ do
            oFadeSlide_
                (xsBoxes_ ++ xsLabels_ ++ ysBoxes_ ++ ysLabels_)
                [O_ resultExpr]
                d

    wait 1

    fork showTypeSig
    wait 0.25

    showFuncDef
    wait 0.5
    showXs
    wait 0.5
    moveXs
    wait 0.5
    showYs
    wait 0.5
    fork moveYs
    moveFuncDef
    wait 0.5
    snapXsYs
    highlightResult
    wait 1.5
    showResultExpr

    wait 3

{-
-------------------------------------------------------------------------------
    Animation-specific functions and values
-------------------------------------------------------------------------------
-}

typeSigChunks :: [Text]
typeSigChunks = _tail . mapped %~ (" " <>) $ ["(++)", "::", "[a] -> [a] -> [a]"]

funcDefChunks :: [Text]
funcDefChunks = _tail . mapped %~ (" " <>) $ ["xs", "++", "ys"]

{-
-------------------------------------------------------------------------------
    Helper functions and values
-------------------------------------------------------------------------------
-}

env :: Animation -> Animation
env =
    docEnv
    . addStatic (mkBackground bgColor)
    -- . addStatic (withStrokeOpacity 0.25 $ mkBackgroundGrid 1 1)
    -- . addStatic (withStrokeOpacity 0.25 mkBackgroundAxes)

transitions :: Animation -> Animation
transitions = applyE (overEnding 1 fadeOutE) . applyE (overBeginning 1 fadeInE)

prepareAnimation :: Animation -> Animation
prepareAnimation =
    env
    . transitions
    . fitAnimationToSize (maxFrameWidth, maxFrameHeight)

maxFrameWidth :: Double
maxFrameWidth = screenWidth - 1

maxFrameHeight :: Double
maxFrameHeight = screenHeight - 1

fgColor :: String
fgColor = "black"

bgColor :: String
bgColor = "floralwhite"

xsColor :: String
xsColor = "red"

ysColor :: String
ysColor = "blue"

resultColor :: String
resultColor = "magenta"

withDefaultTextScaleStrokeFill :: SVG -> SVG
withDefaultTextScaleStrokeFill =
    withDefaultTextStrokeFill . withDefaultTextScale

withAllSubglyphs :: (SVG -> SVG) -> SVG -> SVG
withAllSubglyphs = withSubglyphs [0 ..]

lockLatexSpaces :: Text -> Text
lockLatexSpaces = T.replace " " "~"

makeListExpr :: [Text] -> Text
makeListExpr =
    (<> "]")
    . ("\\raggedright[" <>)
    . T.intercalate ", "
    . fmap lockLatexSpaces

latexBoldChunksWith :: (SVG -> SVG) -> [Text] -> [SVG]
latexBoldChunksWith transformation =
    centerGroupX
    . latexCfgChunksAlignedYWith
        defaultBoldTextCfg
        (transformation . withDefaultTextScaleStrokeFill)

latexBoldWith :: (SVG -> SVG) -> Text -> SVG
latexBoldWith transformation =
    centerX
    . latexCfgAlignedYWith
        defaultBoldTextCfg
        (transformation . withDefaultTextScaleStrokeFill)

latexBoldFgChunks :: [Text] -> [SVG]
latexBoldFgChunks = latexBoldChunksWith (withColor fgColor)

latexBoldFg :: Text -> SVG
latexBoldFg = latexBoldWith (withColor fgColor)

latexRegularWith :: (SVG -> SVG) -> Text -> SVG
latexRegularWith transformation =
    centerX
    . latexCfgAlignedYWith
        defaultRegularTextCfg
        (transformation . withDefaultTextScaleStrokeFill)

lerp' :: Num a => a -> a -> a -> a
lerp' t end start = lerp t (V1 end) (V1 start) ^. _x

oTweenColor :: Object s a -> Duration -> String -> String -> Scene s ()
oTweenColor obj d start end = oTween obj d $ \t -> oContext . mapped %~
    withAllSubglyphs (withTweenedColor start end t)

oFadeSlide :: [Object s a] -> [Object s a] -> Duration -> Scene s ()
oFadeSlide startObjects endObjects d = do
    traverse_ (`oMoveBy` (0, -0.5)) endObjects
    waitOn . traverse_ fork
        $ fmap
            (\obj -> oHideWith obj (d*2/3) oFadeOut)
            startObjects
        ++ fmap
            (\obj -> oShowWith obj (d*2/3) oFadeIn)
            endObjects
        ++ fmap
            (\obj -> oTweenMoveBy obj d (0, 0.5))
            (startObjects ++ endObjects)

oFadeSlide_ :: [O_ s] -> [O_ s] -> Duration -> Scene s ()
oFadeSlide_ startObjects_ endObjects_ d = do
    traverse_ (oApply_ (`oMoveBy` (0, -0.5))) endObjects_
    waitOn . traverse_ fork
        $ fmap
            (oApply_ (\obj -> oHideWith obj (d*2/3) oFadeOut))
            startObjects_
        ++ fmap
            (oApply_ (\obj -> oShowWith obj (d*2/3) oFadeIn))
            endObjects_
        ++ fmap
            (oApply_ (\obj -> oTweenMoveBy obj d (0, 0.5)))
            (startObjects_ ++ endObjects_)
