{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE OverloadedStrings #-}

{-|
    Templates for visualizing lists.

    This module represents a list as a series of boxes, each having a
    label. For example, the list @[1, 2, 4]@ is represented as 3 connected
    boxes, with @1@, @2@ and @4@ written on each box.

    There are templates to represent lists of different lengths, including
    empty lists. List outlines (boxes) and element labels are drawn using
    separate functions. Boxes can have variable widths, though their heights
    are fixed at 1. Labels are drawn using LaTeX, allowing for proper
    superscripts and subscripts.
-}
module Utilities.ListTemplates
    (
    -- * Empty lists
      list0Box
    , emptyListBox
    -- * Building custom lists
    , customListBoxes
    , customListLabels
    , customListBoxesWith
    , customListLabelsWith
    )
    where

import Control.Lens ((?~))
import Data.Text (Text)
import Graphics.SvgTree
    ( Cap (CapSquare)
    , HasDrawAttributes (drawAttributes)
    , strokeLineCap
    )

import Reanimate

import Utilities.LaTeX (defaultRegularTextCfg, latexCfgAlignedYWith)
import Utilities.Misc (distribute1D)
import Utilities.Svg
    ( withColor
    , withDefaultLineStrokeFill
    , withDefaultTextScale
    , withDefaultTextStrokeFill
    )

{-|
    Create a dashed box with the given color to represent an empty list.
-}
list0Box :: String -> SVG
list0Box color =
    withColor color
    . (drawAttributes . strokeLineCap ?~ CapSquare)
    . withDefaultLineStrokeFill
    . withStrokeDashArray [0.1, 0.1]
    $ mkRect 0.5 1

{-|
    Alias for 'list0Box'.
-}
emptyListBox :: String -> SVG
emptyListBox = list0Box

{-|
    Create custom list boxes from the given box widths and color.

    For example, @customListBoxes [1, 2, 4] "blue"@ creates 3 blue boxes of
    widths 1, 2 and 4, respectively. The boxes are laid out head-to-tail
    horizontally, centered at the origin.
-}
customListBoxes :: [Double] -> String -> [SVG]
customListBoxes widths color = customListBoxesWith
    widths
    (withColor color . withDefaultLineStrokeFill)

{-|
    Create custom text labels from the given box widths, color and text values.

    For example, @customListLabels [1, 2, 4] "blue" [Text.pack "a", Text.pack
    "b", Text.pack "c"]@ creates 3 labels "a", "b" and "c" that fit inside the
    boxes produced by @customListBoxes [1, 2, 4] "blue"@.

    (To avoid repeatedly using 'Data.Text.pack', enable the @OverloadedStrings@
    extension.)
-}
customListLabels :: [Double] -> String -> [Text] -> [SVG]
customListLabels widths color = customListLabelsWith
    widths
    ( withColor color
    . withDefaultTextStrokeFill
    . withDefaultTextScale
    . centerX
    )

{-|
    Create custom list boxes from the given box widths and SVG transformation.
    The given transformation is applied to each box before the boxes are laid
    out next to each other.
-}
customListBoxesWith :: [Double] -> (SVG -> SVG) -> [SVG]
customListBoxesWith widths transformation =
    zipWith ($) offsets
    . fmap (transformation . (`mkRect` 1))
    $ widths
    where
        offsets = (`translate` 0) <$> distribute1D widths

{-|
    Create custom text labels from the given box widths, SVG transformation and
    text values. The given transformation is applied to each text label before
    the labels are laid out to fit the given box widths.
-}
customListLabelsWith :: [Double] -> (SVG -> SVG) -> [Text] -> [SVG]
customListLabelsWith widths transformation =
    zipWith ($) offsets
    . fmap (latexCfgAlignedYWith defaultRegularTextCfg transformation)
    where
        offsets = (`translate` 0) <$> distribute1D widths
