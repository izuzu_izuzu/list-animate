{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE TemplateHaskell #-}

{-|
    Custom SVG types.
-}
module Utilities.Svg.Types where

import Control.Lens (makeLenses, (^.))
import Linear (Additive (lerp), V1 (V1), _x)

import Reanimate
import Reanimate.Scene (Renderable (toSVG))

{-|
    A custom SVG type that maintains a constant stroke width, independent of
    the transformations applied to the SVG (e.g. scaling).

    This type mimics the behavior of @vector-effect="non-scaling-stroke"@.
-}
data NonScalingStrokeSVG a = NonScalingStrokeSVG
    { _strokeWidth :: Double
    , _strokedSvg :: a
    }

instance Renderable a => Renderable (NonScalingStrokeSVG a) where
    toSVG (NonScalingStrokeSVG strokeWidth svg) =
        withStrokeWidth strokeWidth . lowerTransformations . toSVG $ svg

{-|
    A custom SVG type that squeezes an SVG inside a bounding area. This is
    mostly intended for fitting a text SVG into a container (e.g. a rectangle).
-}
data TextContainerSVG a = TextContainerSVG
    { _containerSize :: (Double, Double)
    , _textAreaSize :: (Double, Double)
    , _containmentExtent :: Double
    , _textSvg :: a
    }

instance Renderable a => Renderable (TextContainerSVG a) where
    toSVG (TextContainerSVG (cw, ch) (aw, ah) t svg) =
        scaleXY (lerp' t wRatio 1) (lerp' t hRatio 1) $ toSVG svg
        where
            aw' = case aw of 0 -> cw; _ -> aw
            ah' = case ah of 0 -> ch; _ -> ah
            wRatio = min 1 (cw/aw')
            hRatio = min 1 (ch/ah')
            lerp' t' end start = lerp t' (V1 end) (V1 start) ^. _x

makeLenses ''NonScalingStrokeSVG
makeLenses ''TextContainerSVG
