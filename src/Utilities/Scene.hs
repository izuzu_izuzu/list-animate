{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RankNTypes #-}

{-|
    Functions related to 'Scene's and 'Object's.
-}
module Utilities.Scene
    ( oNewWithSvgPosition
    , oNewWithSvgPositionX
    , oNewWithSvgPositionY
    , oMoveTo
    , oMoveToX
    , oMoveToY
    , oMoveBy
    , oClone
    , oTweenMoveTo
    , oTweenMoveToX
    , oTweenMoveToY
    , oTweenMoveBy
    , Utilities.Scene.oShowWith
    , Utilities.Scene.oHideWith
    , (+..)
    , (-..)
    , (>.)
    , (>..)
    , (->.)
    , (->..)
    , (!.)
    , (!..)
    , oWithEasing
    , oShowAsGroupWith
    , oRender
    , oCloneModifyVal
    , O_ (..)
    , oApply_
    )
    where

import Control.Lens ((%~), (+~), (.~))
import Control.Monad (zipWithM_)
import Data.Foldable (traverse_)
import Linear (Additive (lerp), V2 (V2))

import Reanimate
import Reanimate.Scene
    ( Object
    , ObjectData
    , Renderable
    , oContext
    , oEasing
    , oHide
    , oHideWith
    , oMargin
    , oModify
    , oNew
    , oOpacity
    , oRead
    , oSVG
    , oScale
    , oScaleOrigin
    , oShow
    , oShowWith
    , oShown
    , oTranslate
    , oTween
    , oValue
    , oZIndex
    )

import Utilities.Svg (svgCenter)

{-|
    Create a new 'Reanimate.Scene.Object' from an SVG, such that the position
    of the SVG is transferred to the object. This way, all translations can be
    handled using the object's position, without having to manipulate the inner
    SVG.

    For example, if @svg@ is centered at (3, 4), then the new object created by
    @oNewWithSvgPosition svg@ has position (3, 4), and the contained SVG is
    centered at (0, 0).
-}
oNewWithSvgPosition :: SVG -> Scene s (Object s SVG)
oNewWithSvgPosition svg = do
    let (locX, locY) = svgCenter svg
    obj <- oNew $ center svg
    oModify obj $ oTranslate .~ V2 locX locY
    pure obj

{-|
    Create a new 'Reanimate.Scene.Object' from an SVG, such that the /x/-position
    of the SVG is transferred to the object.

    For example, if @svg@ is centered at (3, 4), then the new object created by
    @oNewWithSvgPosition svg@ has position (3, 0), and the contained SVG is
    centered at (0, 4).
-}
oNewWithSvgPositionX :: SVG -> Scene s (Object s SVG)
oNewWithSvgPositionX svg = do
    let (locX, _) = svgCenter svg
    obj <- oNew $ centerX svg
    oModify obj $ oTranslate .~ V2 locX 0
    pure obj

{-|
    Create a new 'Reanimate.Scene.Object' from an SVG, such that the /y/-position
    of the SVG is transferred to the object.

    For example, if @svg@ is centered at (3, 4), then the new object created by
    @oNewWithSvgPosition svg@ has position (0, 4), and the contained SVG is
    centered at (3, 0).
-}
oNewWithSvgPositionY :: SVG -> Scene s (Object s SVG)
oNewWithSvgPositionY svg = do
    let (_, locY) = svgCenter svg
    obj <- oNew $ centerY svg
    oModify obj $ oTranslate .~ V2 0 locY
    pure obj

oMoveTo' :: (Double, Double) -> Double -> ObjectData a -> ObjectData a
oMoveTo' (x, y) t = oTranslate %~ lerp t (V2 x y)

oMoveToX' :: Double -> Double -> ObjectData a -> ObjectData a
oMoveToX' x t = oTranslate %~ \curr@(V2 _ currY) -> lerp t (V2 x currY) curr

oMoveToY' :: Double -> Double -> ObjectData a -> ObjectData a
oMoveToY' y t = oTranslate %~ \curr@(V2 currX _) -> lerp t (V2 currX y) curr

oMoveBy' :: (Double, Double) -> Double -> ObjectData a -> ObjectData a
oMoveBy' (x, y) t = oTranslate +~ lerp t (V2 x y) 0

{-|
    Clone an object, yielding a new object while keeping the original one
    intact.
-}
oClone :: Renderable a => Object s a -> Scene s (Object s a)
oClone = oCloneModifyVal id

{-|
    Given a function and an object, clone the object and apply the function to
    its internal value, yielding a new object. The original object is
    unmodified.
-}
oCloneModifyVal
    :: (Renderable a, Renderable b)
    => (a -> b)
    -> Object s a
    -> Scene s (Object s b)
oCloneModifyVal fn obj = do
    clone <- oNew . fn =<< oRead obj oValue
    traverse_
        (oModify clone =<<)
        [ (oTranslate .~) <$> oRead obj oTranslate
        , (oContext .~) <$> oRead obj oContext
        , (oMargin .~) <$> oRead obj oMargin
        , (oOpacity .~) <$> oRead obj oOpacity
        , (oShown .~) <$> oRead obj oShown
        , (oZIndex .~) <$> oRead obj oZIndex
        , (oEasing .~) <$> oRead obj oEasing
        , (oScale .~) <$> oRead obj oScale
        , (oScaleOrigin .~) <$> oRead obj oScaleOrigin
        ]
    pure clone

{-|
    Gradually show an object over a set duration using an animator function.
-}
oShowWith :: Object s a -> Duration -> (SVG -> Animation) -> Scene s ()
oShowWith obj d fn = Reanimate.Scene.oShowWith obj $ setDuration d . fn

{-|
    Gradually hide an object over a set duration using an animator function.
-}
oHideWith :: Object s a -> Duration -> (SVG -> Animation) -> Scene s ()
oHideWith obj d fn = Reanimate.Scene.oHideWith obj $ setDuration d . fn

{-|
    Instantaneously move an object to a new position.
-}
oMoveTo :: Object s a -> (Double, Double) -> Scene s ()
oMoveTo obj (x, y) = oModify obj $ oMoveTo' (x, y) 1

{-|
    Instantaneously move an object to a new /x/-position, keeping its
    /y/-position unchanged (i.e. move the object horizontally).
-}
oMoveToX :: Object s a -> Double -> Scene s ()
oMoveToX obj x = oModify obj $ oMoveToX' x 1

{-|
    Instantaneously move an object to a new /y/-position, keeping its
    /x/-position unchanged (i.e. move the object vertically).
-}
oMoveToY :: Object s a -> Double -> Scene s ()
oMoveToY obj y = oModify obj $ oMoveToY' y 1

{-|
    Instantaneously move an object by a set amount.
-}
oMoveBy :: Object s a -> (Double, Double) -> Scene s ()
oMoveBy obj (x, y) = oModify obj $ oMoveBy' (x, y) 1

{-|
    Gradually move an object to a new position over a set duration.
-}
oTweenMoveTo :: Object s a -> Duration -> (Double, Double) -> Scene s ()
oTweenMoveTo obj d (x, y) = oTween obj d $ oMoveTo' (x, y)

{-|
    Gradually move an object to a new /x/-position over a set duration, keeping
    its /y/-position unchanged (i.e. move the object horizontally).
-}
oTweenMoveToX :: Object s a -> Duration -> Double -> Scene s ()
oTweenMoveToX obj d x = oTween obj d $ oMoveToX' x

{-|
    Gradually move an object to a new /y/-position over a set duration, keeping
    its /x/-position unchanged (i.e. move the object vertically).
-}
oTweenMoveToY :: Object s a -> Duration -> Double -> Scene s ()
oTweenMoveToY obj d y = oTween obj d $ oMoveToY' y

{-|
    Gradually move an object by some amount over a set duration.
-}
oTweenMoveBy :: Object s a -> Duration -> (Double, Double) -> Scene s ()
oTweenMoveBy obj d (x, y) = oTween obj d $ oMoveBy' (x, y)

{-|
    An operator version of 'Utilities.Scene.oShowWith'.
-}
(+..) :: Object s a -> Duration -> (SVG -> Animation) -> Scene s ()
(+..) = Utilities.Scene.oShowWith

{-|
    An operator version of 'Utilities.Scene.oHideWith'.
-}
(-..) :: Object s a -> Duration -> (SVG -> Animation) -> Scene s ()
(-..) = Utilities.Scene.oHideWith

{-|
    An operator version of 'oMoveTo'.
-}
(>.) :: Object s a -> (Double, Double) -> Scene s ()
(>.) = oMoveTo

{-|
    An operator version of 'oTweenMoveTo'.
-}
(>..) :: Object s a -> Duration -> (Double, Double) -> Scene s ()
(>..) = oTweenMoveTo

{-|
    An operator version of 'oMoveBy'.
-}
(->.) :: Object s a -> (Double, Double) -> Scene s ()
(->.) = oMoveBy

{-|
    An operator version of 'oTweenMoveBy'.
-}
(->..) :: Object s a -> Duration -> (Double, Double) -> Scene s ()
(->..) = oTweenMoveBy

{-|
    An operator version of 'oModify'.
-}
(!.) :: Object s a -> (ObjectData a -> ObjectData a) -> Scene s ()
(!.) = oModify

{-|
    An operator version of 'oTween'.
-}
(!..)
    :: Object s a
    -> Duration
    -> (Double -> ObjectData a -> ObjectData a)
    -> Scene s ()
(!..) = oTween

{-|
    Temporarily apply an easing function to some objects for a scene, then
    restore their original easing functions once the scene ends.
-}
oWithEasing :: Signal -> [Object s a] -> Scene s b -> Scene s b
oWithEasing easing objects actions = do
    oldEasings <- traverse (`oRead` oEasing) objects
    traverse_ (`oModify` (oEasing .~ easing)) objects
    output <- actions
    zipWithM_ (\ease obj -> oModify obj $ oEasing .~ ease) oldEasings objects
    pure output

{-|
    Produce the SVG output of an object.
-}
oRender :: Object s a -> Scene s SVG
oRender obj = do
    _oContext <- oRead obj oContext
    _oOpacity <- oRead obj oOpacity
    _oSVG <- oRead obj oSVG
    _oScale <- oRead obj oScale
    _oScaleOrigin <- oRead obj oScaleOrigin
    _oTranslate <- oRead obj oTranslate
    let
        uncurryV2 fn (V2 a b) = fn a b
        oScaleApply =
            uncurryV2 translate (negate _oScaleOrigin)
            . scale _oScale
            . uncurryV2 translate _oScaleOrigin
    pure
        . uncurryV2 translate _oTranslate
        . oScaleApply
        . withGroupOpacity _oOpacity
        . mkGroup
        $ [_oContext _oSVG]

{-|
    Show multiple objects as if they were part of the same SVG group.
-}
oShowAsGroupWith ::
    [Object s a] -> Duration -> (SVG -> Animation) -> Scene s ()
oShowAsGroupWith objs d fn = do
    groupObj <- oNew . mkGroup =<< traverse oRender objs
    Utilities.Scene.oShowWith groupObj d fn
    oHide groupObj
    traverse_ oShow objs

{-|
    An existential data type for hiding the internal value type of an
    object. This can be used to manipulate multiple objects with different
    internal types in the same scene, as long as no part of the scene needs to
    know those types.
-}
data O_ s = forall a. Renderable a => O_ {unwrapObject :: Object s a}

{-|
    Apply a scene-generating function to an object wrapped in 'O_'. The given
    function must be able to work with objects of any internal value type.
-}
oApply_ :: (forall a. Object s a -> Scene s ()) -> O_ s -> Scene s ()
oApply_ func (O_ obj) = func obj
